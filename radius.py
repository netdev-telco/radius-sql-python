#!/usr/bin/python3
# coding: utf-8

from argparse import ArgumentParser
import mysql.connector


parser = ArgumentParser()
subparser = parser.add_subparsers(dest='command')

show = subparser.add_parser('show', help="Show radius production")
show.add_argument('-l', '--login', help="Show details of login", required=True)

add = subparser.add_parser('add', help="Add radius production")
add.add_argument('-l', '--login', help="Login of client", metavar=('login'), required=True)
add.add_argument('-p', '--password', help="Password of client", metavar=('password'), required=True)
add.add_argument('-a', '--address', help="IP Address to delivered at login", metavar=('address'), required=True)

delete = subparser.add_parser('delete', help="Delete radius production")
delete.add_argument('-l', '--login', help="Login to delete", metavar=('login'), required=True)

append = subparser.add_parser('append', help="Append options radius production")
append.add_argument('-l', '--login', help="Login to delete", metavar=('login'), required=True)
append.add_argument('-p', '--ppp', help="Name of PPP", metavar=('ppp-name'), required=False)
append.add_argument('-r', '--route', help="Network to route on login", metavar=('subnet'), required=False)

args = parser.parse_args()

conn = mysql.connector.connect(host="localhost",user="radius",password="radpass", database="radius")
cursor = conn.cursor()

def do_show_all():
	cursor.execute("SELECT * FROM radcheck")
	print(cursor.fetchall())

def do_check_login_exist(login):
	query = "SELECT * FROM radcheck WHERE username = %s"
	cursor.execute(query, (login,))
	if cursor.fetchone() :
		return True
	
def do_check_login_attribute_radreply(login, attribute):
	query = "SELECT * FROM radreply WHERE username = %s AND attribute = %s"
	cursor.execute(query, (login, attribute))
	if cursor.fetchone() :
		return True
	
def do_check_login_attribute_radreply_op(login, attribute, op):
	query = "SELECT * FROM radreply WHERE username = %s AND attribute = %s AND op = %s"
	cursor.execute(query, (login, attribute, op))
	if cursor.fetchone() :
		return True
	
	
def do_add_radcheck(login, password):
	insert = "INSERT INTO radcheck(username, attribute, value, op) VALUES (%(username)s, %(attribute)s, %(value)s, %(operator)s)"
	data_radcheck = {
		'username' : login,
		'attribute' : "Cleartext-Password",
		'value' : password,
		'operator' : ":="
	}
	cursor.execute(insert, data_radcheck)
	conn.commit()


def do_add_radreply(login, attribute, operator, value):
	insert = "INSERT INTO radreply(username, attribute, value, op) VALUES (%(username)s, %(attribute)s, %(value)s, %(operator)s)"
	data_radcheck = {
		'username' : login,
		'attribute' : attribute,
		'value' : value,
		'operator' : operator
	}
	cursor.execute(insert, data_radcheck)
	conn.commit()

def do_delete_production(login):
	delete_check = "DELETE FROM radcheck WHERE username = %s"
	delete_reply = "DELETE FROM radreply WHERE username = %s"
	cursor.execute(delete_check, (login,))
	cursor.execute(delete_reply, (login,))
	conn.commit()

def do_show_details(login):
	query = "SELECT * FROM radcheck WHERE username = %s"
	cursor.execute(query, (login,))
	data = cursor.fetchone()
	if data :
		query_reply = "SELECT * FROM radreply WHERE username = %s"
		cursor.execute(query_reply, (login,))
		datas_reply = cursor.fetchall()

		print("###---RADCHECK---###")

		print(" - ID Radius : " + str(data[0]))
		print(" - Login : " + data[1])
		print(" - Password : " + data[4])

		print("###---RADREPLY---###")

		for data_reply in datas_reply:
			print(" -> " + data_reply[2] + " " + data_reply[3] + " " + data_reply[4])
	else:
		print("No login match in radius")

if args.command == "show":
	if args.login == "all":
		do_show_all()
	else:
		do_show_details(args.login)
elif args.command == "add":
	if not do_check_login_exist(args.login):
		do_add_radcheck(args.login, args.password)
		do_add_radreply(args.login, "Framed-IP-Address", "=", args.address)
	else:
		print("The login already present in radius !")
elif args.command == "delete":
	if do_check_login_exist(args.login):
		do_delete_production(args.login)
	else:
		print("The login is not present in radius !")		
elif args.command == "append":
	if do_check_login_exist(args.login):
		print(args)
		if args.ppp:
			attribute = "NAS-Port-Id"
			if not do_check_login_attribute_radreply(args.login, attribute):
				do_add_radreply(args.login, attribute, "=", args.ppp)
			else:
				print("Name of login is already present !")
		if args.route:
			attribute = "Framed-Route"
			if args.route.startswith('+'):
				newRoute = args.route.replace('+', '')
				do_add_radreply(args.login, attribute, "+=", newRoute)
			else:
				if not do_check_login_attribute_radreply_op(args.login, attribute, "="):
					do_add_radreply(args.login, attribute, "=", args.route)
				else:
					print("Standard route already present !")
	else:
		print("The login is not present in radius !")		


conn.close()
